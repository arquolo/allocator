﻿#pragma once

#include <cstddef>
#include <memory>
#include <new>

namespace memory {

class stack {
    static constexpr std::align_val_t align{32};

    struct header {
        size_t padding;
    };

    std::unique_ptr<char[]> storage_;
    size_t total_size_;
    size_t head_{};

public:
    stack(size_t);
    stack(stack&) = delete;

    void* allocate(size_t);
    void deallocate();
    void reset();
};

} // namespace memory
