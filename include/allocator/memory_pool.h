﻿#pragma once

#include <cstddef>
#include <memory>
#include <stack>

namespace memory {

class pool {
    static constexpr std::align_val_t align{32};

    std::stack<void*> chunks_;
    std::unique_ptr<char[]> storage_;

    size_t total_size_;
    size_t chunk_size_;
    size_t head_{};

public:
    pool(size_t, size_t);
    pool(pool&) = delete;

    void* allocate() noexcept;
    void deallocate(void*) noexcept;
    void reset() noexcept;
};

} // namespace memory
