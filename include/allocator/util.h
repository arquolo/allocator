﻿#pragma once

#include <cstddef>

namespace util {

size_t pad(size_t base_addr, size_t alignment) noexcept;

size_t
pad_header(size_t base_addr, size_t alignment, size_t header_size) noexcept;

} // namespace util
