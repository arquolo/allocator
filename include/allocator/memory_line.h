﻿#pragma once

#include <cstddef>
#include <memory>
#include <new>

namespace memory {

class linear {
    static constexpr std::align_val_t align{32};

    std::unique_ptr<char[]> storage_;

    size_t total_size_;
    size_t head_{};
    size_t count_{};

public:
    linear(size_t);
    linear(linear&) = delete;

    void* allocate(size_t) noexcept;
    void deallocate() noexcept;
    void reset();
};

} // namespace memory
