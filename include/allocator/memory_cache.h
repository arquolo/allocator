﻿#pragma once

#include <cstddef>
#include <list>
#include <memory>
#include <new>
#include <tuple>

namespace memory {
// TODO: Rewrite using STL, placement-new and aligned-new
/*
class cache {
    enum placement_policy { FIND_FIRST, FIND_BEST };
    static constexpr std::align_val_t align{32};

    struct header {
        size_t block_size;
        size_t padding;
    };

    struct chunk {
        size_t offset;
        size_t size;
    };

    std::unique_ptr<char[]> storage_;
    std::list<chunk> free_list_;
    using node = std::list<chunk>::iterator;

    size_t total_size_;
    size_t used_{};
    placement_policy p_policy_;

    void coalescence(node&);

    auto find(size_t);
    auto find_best(size_t);
    auto find_first(size_t);

public:
    cache(size_t, placement_policy);
    cache(cache&) = delete;

    void* allocate(size_t, size_t = 0);
    void free(void*);
    void reset();
};
*/
} // namespace memory
