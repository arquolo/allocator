﻿import qbs
import qbs.Probes

Product {
    Depends { name: "cpp" }
    cpp.compilerName: "clang"
    cpp.cxxCompilerName: "clang++"
    buildDirectory: "build"

    type: [ "dynamicLibrary" ]
    files: [
        "include/**/*.h",
        "src/**/*.cpp",
    ]
    cpp.includePaths: "./include"

    cpp.commonCompilerFlags: {
        return base.concat([
            "-std=c++17", "-std=gnu++17",
            "-Weverything",
            "-Wno-class-varargs", "-Wno-padded", "-Wno-switch-enum", "-Wno-unused-macros",
            "-Wno-c++98-compat", "-Wno-c++98-compat-pedantic",
            "-Wno-c++14-extensions", "-Wno-c++17-extensions"
        ])
    }
    Properties {
        condition: qbs.buildVariant == "debug"
        cpp.cxxFlags: {
            return base.concat([ "-g", "-Og" ])
        }
    }
    Properties {
        condition: qbs.buildVariant == "release"
        cpp.cxxFlags: {
            return base.concat([ "-O3", "-ffast-math", "-funroll-loops" ])
        }
        cpp.discardUnusedData: true
    }
    Group {
        fileTagsFilter: "dynamicLibrary"
        qbs.install: false
    }
}
