﻿
#include "allocator/memory_line.h"
#include "allocator/util.h"
#include <new>

namespace memory {

linear::linear(size_t total_size)
: storage_(new (align) char[total_size]), total_size_(total_size) {}

void* linear::allocate(size_t size) noexcept {
    const auto padding = util::pad(head_, static_cast<size_t>(align));

    auto const head = head_ + padding + size;
    if (head > total_size_)
        return nullptr;

    ++count_;
    head_ = head;
    return storage_.get() + head_;
}

void linear::deallocate() noexcept {
    if (--count_ == 0)
        reset();
}

void linear::reset() {
    head_ = 0;
}

} // namespace memory
