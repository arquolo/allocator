﻿
#include "allocator/memory_stack.h"
#include "allocator/util.h"

namespace memory {

stack::stack(size_t total_size)
: storage_(new (align) char[total_size]), total_size_(total_size) {}

void* stack::allocate(size_t size) {
    auto const padding
        = util::pad_header(head_, static_cast<size_t>(align), sizeof(header));

    const auto head = head_ + padding + size;
    if (head > total_size_)
        return nullptr;

    auto const head_r = head_ + padding;
    reinterpret_cast<header*>(storage_.get() + head_r - sizeof(header))->padding
        = padding;

    head_ = head;
    return storage_.get() + head_r;
}

void stack::deallocate() {
    auto const hdr
        = reinterpret_cast<header*>(storage_.get() + head_ - sizeof(header));
    head_ -= hdr->padding;
}

void stack::reset() {
    head_ = 0;
}

} // namespace memory
