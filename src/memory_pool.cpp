﻿
#include "allocator/memory_pool.h"
#include <cassert>

namespace memory {

pool::pool(size_t total_size, size_t chunk_size)
: storage_(new (align) char[total_size])
, total_size_(total_size)
, chunk_size_(chunk_size) {
    assert(chunk_size_ >= 8);
    assert(total_size_ % chunk_size_ == 0);
    reset();
}

void* pool::allocate() noexcept {
    assert(size == chunk_size_);

    const auto position = chunks_.top();
    chunks_.pop();

    assert(position != nullptr);

    head_ += chunk_size_;
    return position;
}

void pool::deallocate(void* ptr) noexcept {
    head_ -= chunk_size_;
    chunks_.push(ptr);
}

void pool::reset() noexcept {
    head_ = 0;
    auto const root = storage_.get();
    for (size_t offset = 0; offset < total_size_; offset += chunk_size_)
        chunks_.push(root + offset);
}

} // namespace memory
