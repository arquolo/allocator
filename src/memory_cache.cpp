﻿
#include "allocator/memory_cache.h"
#include "allocator/util.h"
#include <algorithm>
#include <cassert>
#include <limits>

namespace memory {
// TODO: Rewrite using STL, placement-new and aligned-new
/*
cache::cache(size_t total_size, placement_policy p_policy)
: storage_(new (align) char[total_size])
, total_size_(total_size)
, p_policy_(p_policy) {
    reset();
}

void* cache::allocate(size_t size) {
    static constexpr auto allocation_header_size = sizeof(cache::header);

    assert(size >= sizeof(node));

    // Search through the free list for a free block that has enough space to
    // allocate our data
    auto [affected_node, padding] = find(size);
    assert(affected_node != nullptr);

    const auto alignment_padding = padding - allocation_header_size;
    const auto required_size = size + padding;

    const auto rest = affected_node->block_size - required_size;

    if (rest > 0) {
        // We have to split the block into the data block and a free block of
        // size 'rest'
        auto new_free_node = reinterpret_cast<node*>(
            reinterpret_cast<char*>(affected_node) + required_size);
        new_free_node->block_size = rest;
        free_list_.insert(affected_node, new_free_node);
    }
    free_list_.remove(affected_node);

    // Setup data block
    const auto header_ptr
        = reinterpret_cast<char*>(affected_node) + alignment_padding;
    const auto data_ptr = header_ptr + allocation_header_size;
    reinterpret_cast<header*>(header_ptr)->block_size = required_size;
    reinterpret_cast<header*>(header_ptr)->padding
        = static_cast<char>(alignment_padding);

    used_ += required_size;
    return data_ptr;
}

auto cache::find(size_t size) {
    switch (p_policy_) {
    case FIND_FIRST:
        return find_first(size);
    case FIND_BEST:
        return find_best(size);
    }
    __builtin_unreachable();
}

auto cache::find_first(size_t size) {
    // Return the first free block with a size >= than given size
    size_t padding{};
    auto r = std::find_if(free_list_.begin(), free_list_.end(), [&](auto it) {
        padding = util::pad_header(
            it.offset, static_cast<size_t>(align), sizeof(header));
        const auto required_space = size + padding;
        return (it.size >= required_space);
    });
    return std::make_tuple(r, padding);
}

auto cache::find_best(size_t size) {
    // Iterate WHOLE list keeping a pointer to the best fit
    size_t padding{};
    auto smallest_diff = std::numeric_limits<size_t>::max();
    auto best = std::min_element(
        free_list_.begin(), free_list_.end(), [&](auto const& x) {
            padding = util::pad_header(
                x.offset, static_cast<size_t>(align), sizeof(header));
            const auto required_space = size + padding;
            if (x.size >= required_space)
                if (x.size - required_space < smallest_diff)
                    return x;
        });
    return std::make_tuple(best, padding);
}

void cache::free(void* ptr) {
    // Insert it in a sorted position by the address number
    const auto current_address = reinterpret_cast<size_t>(ptr);
    const auto header_address = current_address - sizeof(header);
    const auto hdr = reinterpret_cast<header*>(header_address);

    auto free_node = reinterpret_cast<node*>(header_address);
    free_node->size = hdr->block_size + static_cast<size_t>(hdr->padding);
    free_node->next = nullptr;

    node* it_prev = nullptr;
    node* it = free_list_.head;
    while (it != nullptr && ptr >= it) {
        it_prev = it;
        it = it->next;
    }
    if (it != nullptr)
        free_list_.insert(it_prev, free_node);

    used_ -= free_node->block_size;

    // Merge contiguous nodes
    coalescence(free_node);
}

void cache::coalescence(node& node) {
    if (node != free_list_.end()) {
        auto next = node;
        ++next;
        if (node->offset + node->size == next->offset) {
            node->size += next->size;
            free_list_.erase(next);
        }
    }
    if (node != free_list_.begin()) {
        auto prev = node;
        --prev;
        if (prev->offset + prev->size == node->offset) {
            prev->size += node->size;
            free_list_.erase(node);
        }
    }
}

void cache::reset() {
    used_ = 0;

    free_list_.clear();
    free_list_.emplace_back(size_t{}, total_size_);
}
*/
} // namespace memory
