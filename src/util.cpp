﻿
#include "allocator/util.h"
#include <memory>

namespace util {

size_t pad(size_t base_addr, size_t align) noexcept {
    if (base_addr % align == 0)
        return 0;
    return align * (base_addr / align + 1) - base_addr;
}

size_t pad_header(size_t base_addr, size_t align, size_t header_size) noexcept {
    auto padding = pad(base_addr, align);
    auto needed_space = header_size;

    if (padding >= needed_space)
        return padding;

    // Header does not fit - Calculate next aligned address that header fits
    needed_space -= padding;

    // How many alignments I need to fit the header
    if (needed_space % align > 0)
        padding += align * (1 + (needed_space / align));
    else
        padding += needed_space;
    return padding;
}

} // namespace util
